package main.com.task.handlers;

import main.com.task.models.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import static java.lang.Math.toIntExact;

import static java.nio.file.StandardCopyOption.*;


public class Downloader implements Callable<Void> {
    private String url;
    private HashSet<File> toDownload;
    private long speedLimit;
    private String outputPath;

    public Downloader(String url, HashSet<File> toDownload, long speedLimit, String outputPath) {
        this.url = url;
        this.toDownload = toDownload;
        this.speedLimit = speedLimit;
        this.outputPath = outputPath;
    }

    private String generateBufferName(){
        return UUID.randomUUID().toString();
    }

    private boolean downloadBytes(FileOutputStream fileOutputStream,ReadableByteChannel from,int startPosition,int number) throws IOException {
        long downloadedBytes=fileOutputStream.getChannel().transferFrom(from, startPosition,number);

        if(downloadedBytes<number){
            return true;
        }

        return false;
    }

    private void checkLimit(long startTime,long finishTime) {
        long workTime=TimeUnit.MILLISECONDS.convert(finishTime, TimeUnit.NANOSECONDS)-TimeUnit.MILLISECONDS.convert(startTime, TimeUnit.NANOSECONDS);
        if(workTime>1000){
            return;
        }
        try {
            TimeUnit.MILLISECONDS.sleep(1000-workTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void download(String bufferName,long speedLimit) throws IOException {
        URL website = new URL(url);
        ReadableByteChannel readableByteChannel = Channels.newChannel(website.openStream());
        FileOutputStream fileOutputStream = new FileOutputStream(bufferName);
        boolean alreadyDownloaded=false;
        int position=0;
        while(!alreadyDownloaded){
            long startTime = System.nanoTime();
            alreadyDownloaded=downloadBytes(fileOutputStream,readableByteChannel,position,toIntExact(speedLimit));
            position+=speedLimit;
            long finishTime=System.nanoTime();
            checkLimit(startTime,finishTime);
        }
        fileOutputStream.close();
        readableByteChannel.close();

    }

    private void cleanBuffer(String bufferName){
        try {
            Files.delete(Paths.get(bufferName));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void copyFiles(HashSet<File> downloadList,String bufferName)  {
        String fileOutputPath;
        for(File file:downloadList) {
            try {
                Files.createDirectories(Paths.get(outputPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileOutputPath=outputPath+'/'+file.getFileName();
            try {
                Files.copy(Paths.get(bufferName), Paths.get(fileOutputPath), REPLACE_EXISTING);
            } catch (IOException e) {
                System.out.print(e.toString());
                e.printStackTrace();
            }
        }

    }
    @Override
    public Void call() throws Exception {
        String bufferName=generateBufferName();
        download(bufferName,speedLimit);
        copyFiles(toDownload,bufferName);
        cleanBuffer(bufferName);
        return null;
    }

    public HashSet<File> getToDownload() {
        return toDownload;
    }

    public String getUrl() {
        return url;
    }

    public long getSpeedLimit() {
        return speedLimit;
    }


}
