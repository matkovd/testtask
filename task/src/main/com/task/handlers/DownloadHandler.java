package main.com.task.handlers;

import main.com.task.models.File;
import main.com.task.models.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class DownloadHandler {
    private HashMap<String,HashSet<File>> preProcess(ArrayList<File> downloadList){
        HashMap<String,HashSet<File>> res=new HashMap<>();
        for(File file:downloadList){
            if(!res.containsKey(file.getUrl())){
                res.put(file.getUrl(),new HashSet<>());
            }
            res.get(file.getUrl()).add(file);
        }
        return res;
    }
    public void download(ArrayList<File> downloadList, Request request){
        HashMap<String,HashSet<File>> data = preProcess(downloadList);
        ArrayList<Callable<Void>> tasks=new ArrayList<>();
        for(String key:data.keySet()){
            tasks.add(new Downloader(key,data.get(key),request.getSpeedLimit(),request.getOutputPath()));
        }
        ExecutorService executorService = Executors.newFixedThreadPool(request.getThreadNumber());
        try {
            List<Future<Void>> lst=executorService.invokeAll(tasks);
            executorService.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
