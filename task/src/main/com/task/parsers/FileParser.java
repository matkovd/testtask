package main.com.task.parsers;

import main.com.task.models.File;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class FileParser{
    private static Logger log = Logger.getLogger(FileParser.class.getName());

    public ArrayList<File> parse(String filePath) {
        ArrayList<File> result=new ArrayList<>();


        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line = br.readLine();
            while (line != null) {
                String[] row=line.split(" ");
                if(row.length!=2){
                    log.info("bad line,passing");
                    continue;
                }
                result.add(new File(row[0],row[1]));
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("file not found");
        } catch (IOException e) {
            throw new IllegalArgumentException("bad file");
        }
        return result;
    }
}
