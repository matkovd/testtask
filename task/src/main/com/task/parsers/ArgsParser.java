package main.com.task.parsers;

import main.com.task.models.Request;

public class ArgsParser {
    public Request parse(String[] args) throws  IllegalArgumentException {
        int threadNumber=0;
        String filePath=null;
        String outputPath=null;
        String speedLimit=null;
        if(args.length<8){
            throw new IllegalArgumentException();
        }
        if(args.length%2!=0){
            throw new IllegalArgumentException();
        }
        for(int index=0;index<args.length;index+=2){
            switch (args[index]) {
                case "-f":
                    filePath=args[index+1];
                    break;
                case "-o":
                    outputPath=args[index+1];
                    break;
                case "-n":
                    threadNumber=Integer.parseInt(args[index+1]);
                    break;
                case "-l":
                    speedLimit=args[index+1];
                    break;

            }

        }
        return new Request(filePath,threadNumber,speedLimit,outputPath);
    }

}
