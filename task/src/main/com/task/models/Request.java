package main.com.task.models;


public class Request {
    private String filePath;
    private int threadNumber;
    private long speedLimit;
    private String outputPath;

    private void validate(String filePath, int threadNumber, String speedLimit, String outputPath) throws IllegalArgumentException {
        if(filePath==null){
            throw new IllegalArgumentException();
        }
        if(filePath.isEmpty()){
            throw new IllegalArgumentException();
        }
        if(outputPath==null){
            throw new IllegalArgumentException();
        }
        if(threadNumber<1) {
            throw new IllegalArgumentException();
        }
        if(!((speedLimit.endsWith("k"))||speedLimit.endsWith("m"))){
            throw new IllegalArgumentException();
        }
        if(speedLimit.length()<2){
            throw new IllegalArgumentException();
        }
    }

    private long calcLimit(String speedLimit){
        long limit=Long.parseLong(speedLimit.substring(0,speedLimit.length()-1));
        if(speedLimit.endsWith("k")){
            limit*=1024;
        }
        if(speedLimit.endsWith("m")){
            limit*=1024*1024;
        }
        return limit;
    }
    public Request(String filePath, int threadNumber, String speedLimit, String outputPath) {
        validate(filePath,threadNumber,speedLimit,outputPath);
        this.filePath = filePath;
        this.threadNumber = threadNumber;
        this.speedLimit = calcLimit(speedLimit);
        this.outputPath = outputPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public int getThreadNumber() {
        return threadNumber;
    }

    public long getSpeedLimit() {
        return speedLimit;
    }

    public String getOutputPath() {
        return outputPath;
    }
}
