package main.com.task.models;

import java.util.Objects;

public class File {
    private String url;
    private String fileName;

    public File(String url, String fileName) {
        this.url = url;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getUrl() {
        return url;
    }

    public boolean equals(File file) {
        return (Objects.equals(this.fileName, file.fileName))&&(Objects.equals(this.url, file.url));
    }

    public String toString() {
        return "filename:"+this.fileName+"at url:"+this.url;
    }
}
