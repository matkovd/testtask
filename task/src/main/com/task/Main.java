package main.com.task;


import main.com.task.handlers.DownloadHandler;
import main.com.task.models.File;
import main.com.task.models.Request;
import main.com.task.parsers.ArgsParser;
import main.com.task.parsers.FileParser;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        DownloadHandler downloadHandler=new DownloadHandler();
        ArgsParser argsParser=new ArgsParser();
        FileParser fileParser=new FileParser();
        Request request=argsParser.parse(args);
        ArrayList<File> files=fileParser.parse(request.getFilePath());
        downloadHandler.download(files,request);
    }
}
